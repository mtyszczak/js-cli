/**
 * config.ts - Project configuration and assets
 */

const initTimestamp = Date.now();

const terminalPrefix = `>>> `;

const name = "js-cli example";
const version = "0.1";

export default {
  initTimestamp,
  terminalPrefix,
  name,
  version
}
