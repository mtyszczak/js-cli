/**
 * gethelp.js - CommandModule - gethelp - Get help for specific command
 * @since 0.0.1
 * @author LyingCarpet
 */

import { ArgumentType } from "../../../../src/arguments";
import { CommandModule, CommandManager, Command } from "../../../../src/commands";
import { GroupManager } from "../../../../src/groups";

import chalk = require('chalk');
import { staticAssert } from "../../../../libraries/js-helpers/src/assert";
import { deserializeCommandUsage } from "../../../../src/commands_helper";

export default {
  name: "gethelp",
  description: "Get help for a specific command",
  argsList: [ { name: "commandName", type: "string" } ],
  returns: { type: "string" } as ArgumentType,
  invoke: ( commandName:string ):string => {
    staticAssert( CommandManager.has( commandName ), "Help for the given command does not exist", { commandName } );
    const command_:Command = CommandManager.get( commandName );
    let ret = ``;

    if( command_.group().length > 0 ) {
      ret += `In group: `;
      staticAssert( GroupManager.has( command_.group() ), "Group assigned to the given command does not exist", { commandName } );
      ret += chalk.italic( GroupManager.get( command_.group() ).name );
      ret += '\n\n';
    }

    ret += chalk.bold.blue( command_.name() );
    ret += chalk.blue( ` - ${command_.description()}\n` );

    ret += "Usage:\n  ";
    ret += deserializeCommandUsage( command_ );
    ret += '\n';

    return ret;
  }
} as CommandModule;