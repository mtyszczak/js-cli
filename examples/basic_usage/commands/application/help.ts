/**
 * help.js - CommandModule - help - Displays bot help
 * @since 0.0.1
 * @author LyingCarpet
 */

import { ArgumentType } from "../../../../src/arguments";
import { CommandModule } from "../../../../src/commands";
import { GroupManager, GroupManagerStorageIndexType } from "../../../../src/groups";
import config from "../../assets/config";

import chalk = require('chalk');
import { deserializeCommandUsage } from "../../../../src/commands_helper";

export default {
  name: "help",
  description: "Displays a brief help for all of the commands",
  argsList: [],
  returns: { type: "string" } as ArgumentType,
  invoke: ():string => {
    let ret = chalk.bold.cyan( `${config.name} v${config.version}\n` );

    let groupNames:Array< string > = GroupManager.getStorageKeys();
    for( const groupName of groupNames ) {
      let group:GroupManagerStorageIndexType =  GroupManager.get( groupName );

      ret += '\n';
      ret += chalk.bold.blue( group.name );
      if( group.description.length > 0 )
        ret += chalk.blue( ` - ${group.description}` );
      ret += '\n';

      for( const command of group.commands ) {
        ret += "  ";
        ret += deserializeCommandUsage( command );
        ret += '\n';
      }
    }

    return ret;
  }
} as CommandModule;