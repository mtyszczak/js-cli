/**
 * exit.js - CommandModule - exit - Exits from the bot interactive mode
 * @since 0.0.1
 * @author LyingCarpet
 */

import { ArgumentReflector, ArgumentType } from "../../../../src/arguments";
import { CommandModule } from "../../../../src/commands";

/**
 * Basic exit_t inerface for exitting
 */
export class exit_t {
  public constructor( code:number ) { this.code = code; }
  code: number;
  readonly value:string = "\x04";
}

ArgumentReflector.set( "exit_t", {
   isValid( _data:exit_t ):boolean { return true; },
   toString( data:exit_t ):string { return JSON.stringify({ code: data.code }); },
   fromString( data:string ):exit_t { return new exit_t( JSON.parse( data ).code ); }
} );

export default {
  name: "exit",
  description: "Exits from the bot interactive mode",
  argsList: [],
  returns: { type: "exit_t" } as ArgumentType,
  invoke: ():exit_t => {
    process.kill( process.pid, "SIGINT" ); // Kill the current process
    return new exit_t( 0 );
  }
} as CommandModule;
