/**
 * application.js - GroupModule - Application - options and helper commands
 * @since 0.0.1
 * @author LyingCarpet
 */

import { retrieveCommandsFromGroup } from "../../../src/commands_helper";
import { GroupModule } from "../../../src/groups";

export default {
  name: "Application",
  description: "options and helper commands",
  commands: retrieveCommandsFromGroup( __dirname, __filename )
} as GroupModule;
