/**
 * tui.ts - terminal user interface for js-cli example ripper interactive mode
 *
 * @since 0.0.1
 * @license GPLv3
 */

import config from './assets/config';
import logger = require("node-color-log");
import query = require( "cli-interact" );
import chalk = require('chalk');
import { CommandManager } from '../../src/commands';
import { staticAssert } from '../../libraries/js-helpers/src/assert';

/**
 * Gets line from the user (prompts)
 * @returns {string} user input
 */
const getLine = ():string => query.question( chalk.bold( config.terminalPrefix ) );

/**
 * Processes user input
 * @param {string} input input to be processed
 * @returns {boolean} either true or false if user wants to exit
 */
const processInput = ( input:string ):boolean => {
  try {
    if( input.length === 0 ) return false;

    const command = CommandManager.parse( input );

    if( CommandManager.has( command.name ) ) {
      const ret = CommandManager.invoke( command.name, ...command.args );
      logger.log( ret );
      return CommandManager.get( command.name ).returns().type === "exit_t";
    } else {
      staticAssert( CommandManager.has( "help" ), "Help command is required on the CommandManager" );
      logger.warn( "Command not found. Use `help` in order to display all of the available commands" );
    }
  } catch( e ) {
    logger.error( (e as Error).message );
  }

  return false;
};

export default {
  getLine,
  processInput
}