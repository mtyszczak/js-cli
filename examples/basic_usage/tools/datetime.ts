/**
 * datetime.ts - date and time helpers for datezone_ripper
 *
 * @since 0.0.1
 * @license GPLv3
 */

/**
 * Fills the number with given number of zeros if needed
 * Exaple of number=10 and fill=3: 010
 *
 * @param {number} number number to be formatted
 * @param {number} fill number of zeros if needed
 * @returns {string} formatted string
 */
const fillNumber = ( _number:number, fill:number ):string => {
  let stringRep:string = _number.toString();
  let negativeFlag:boolean = false;
  // Cut `-` if number is negative to support filling of the negative numbers:
  if( stringRep.charAt(0) === '-' ) {
    stringRep.substr( 1 );
    negativeFlag = true;
  }

  for( let i = fill - stringRep.length; i > 0; --i )
    stringRep = '0' + stringRep;

  if( negativeFlag )
    stringRep = '-' + stringRep;
  return stringRep;
};

/**
 * Converts current time millis to the readable string
 * @param {number} millis timestamp to be formatted
 * @returns {string} readable time range format
 */
const toRangeTimeString = ( millis:number ):string => {
  let ms:number = millis % 1000, s:number = 0, m:number = 0, h:number = 0, d:number = 0;
  if( ( millis /= 1000 ) > 0 ) s = Math.round( millis % 60 );
  if( ( millis /= 60 ) > 0 ) m = Math.round( millis % 60 );
  if( ( millis /= 60 ) > 0 ) h = Math.round( millis % 24 );
  if( ( millis /= 24 ) > 0 ) d = Math.round( millis );

  const dStr = fillNumber(d,2);
  const hStr = fillNumber(h,2);
  const mStr = fillNumber(m,2);
  const sStr = fillNumber(s,2);
  const msStr = fillNumber(ms,3);
  if( d > 0 ) return `${dStr}d ${hStr}:${mStr}:${sStr}.${msStr}`;
  else if( h > 0 ) return `0d ${hStr}:${mStr}:${sStr}.${msStr}`;
  else if( m > 0 ) return `0d 00:${mStr}:${sStr}.${msStr}`;
  else if( s > 0 ) return `0d 00:00:${sStr}.${msStr}`;
  else return `0d 00:00:00.000`;
};

export default {
  fillNumber,
  toRangeTimeString
};
