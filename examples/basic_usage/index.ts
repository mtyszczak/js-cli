/**
 * index.ts - Init script
 *
 * @since 0.0.1
 * @license GPLv3
 */

import logger = require("node-color-log");
import death = require("death");

import config from './assets/config';
import datetime from './tools/datetime';
import tui from './tui';
import { registerAllCommands } from "../../src/commands_helper";

// Listen for connection close
death( signal => {
  logger.info( `[${signal}] Shutting down program after ${datetime.toRangeTimeString( Date.now() - config.initTimestamp )}` );
  process.exit( 0 );
} );

logger.debug( "Registering commands..." );
registerAllCommands( __dirname, "./commands" );

logger.debug( "Entering main program loop..." );

// Main program loop:
while( !tui.processInput( tui.getLine() ) );
