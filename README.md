# js-cli

[![pipeline status](https://gitlab.com/mtyszczak/js-cli/badges/master/pipeline.svg)](https://gitlab.com/mtyszczak/js-cli/-/commits/master)
[![coverage report](https://gitlab.com/mtyszczak/js-cli/badges/master/coverage.svg)](https://gitlab.com/mtyszczak/js-cli/-/commits/master)
[![License GNU GPL v3](https://img.shields.io/badge/License-GNU%20GPL%20v3-red.svg)](LICENSE)
[![Version 0.0.1](https://img.shields.io/badge/Version-0.0.1-abc.svg)](https://gitlab.com/mtyszczak/js-cli/-/commits/master)

command line interface implementation in TypeScript

## Compiling
Initialize libraries:
```
git submodule update --init --recursive --progress
```
Run `build.sh` script located in the root directory of this project to compile the game into the javascript. Use `--help` option to display builder help. You can also specify the output directory by using `--outdir` option.

## Running examples
First you have to [compile](#Compiling) with `--with-examples`

In order to run this project use node:
```
node $OUTDIR/examples/$EXAMPLE_NAME
```
where `$OUTDIR` is your output directory specified during compilation and `$EXAMPLE_NAME` is example you want to run

## Documentation
You can use [Repo Wiki](https://gitlab.com/mtyszczak/js-cli/-/wikis/home) or generated documentation available in the `wiki` directory
To regenerate documentation run `build.sh -d`

## License
Distributed under the GNU GENERAL PUBLIC LICENSE Version 3
See [LICENSE.md](LICENSE.md)