#!/bin/bash

OUTDIR=.
VERSION="0.0.1"
TSC_OPTIONS="--alwaysStrict --target ES2017 --module commonjs --pretty --skipLibCheck --experimentalDecorators"

# Internal flags
flag_gen_docs=
flag_run_tests=
flag_skip_building=
flag_use_docker=
flag_with_examples=
want_help=

bin_docker_image="docker run --rm --volume $(pwd):/app sandrokeil/typescript"

tsc_bin=tsc
npx_bin=npx
npm_bin=npm
node_bin=node

for option
do
    case $option in

    -help | --help | -h)
      want_help=yes ;;

    -generate-docs | --generate-docs | -d)
      flag_gen_docs=yes ;;

    -with-examples | --with-examples | -d)
      flag_with_examples=yes ;;

    -use-docker | --use-docker )
      flag_use_docker=yes ;;

    -skip-building | --skip-building | -s)
      flag_skip_building=yes ;;

    -outdir=* | --outdir=*)
      OUTDIR=`expr "x$option" : "x-*outdir=\(.*\)"`
      ;;

    -run-tests | --run-tests )
      flag_run_tests=yes
      ;;

    -*)
      { echo "error: unrecognized option: $option
Try \`$0 --help' for more information." >&2
      { (exit 1); exit 1; }; }
      ;;

    esac
done

if test "x$want_help" = xyes; then
  cat <<EOF
\`./build.sh\` builds the datezone-ripper program and libraries

Usage: $0 [OPTION]...

Defaults for the options are specified in brackets.

Configuration:
  -h, --help                   display this help and exit
  -d, --generate-docs          regenerate documentation

Build options:
  -s, --skip-building          skips building javascript source (exit after
                               running all of the tests)
  --use-docker                 uses docker sandrokeil/typescript image instead of
                               local typescript and nodejs commands
  --with-examples              compiles all of the examples

Installation directories:
  --outdir=OUTDIR              install helper-js into the given OUTDIR

Testing
  --run-tests                  runs tests in tests directory

EOF
fi
test -n "$want_help" && exit 0

my_dir=$(dirname "$0")

if test "x$flag_use_docker" = xyes; then
cat <<EOF
Using docker image: $bin_docker_image
EOF
  tsc_bin="$bin_docker_image $tsc_bin"
  npm_bin="$bin_docker_image $npm_bin"
  npx_bin="$bin_docker_image $npx_bin"
  node_bin="$bin_docker_image $node_bin"

fi

if test "x$flag_gen_docs" = xyes; then

  cat <<EOF
Regenerating documentation...

EOF

if test "x$flag_use_docker" != xyes; then
  # Find npm and nodejs
  result=`npm -v > /dev/null 2>&1`
  if [ "$?" -ne "0" ]; then
    cat <<EOF
npm not found! Try installing it with \`sudo apt install npm\` Exitting...

EOF
    exit 1
  fi
  result=`node -v > /dev/null 2>&1`
  if [ "$?" -ne "0" ]; then
    cat <<EOF
node not found! Try installing it with \`sudo apt install nodejs\` Exitting...

EOF
    exit 1
  fi
fi

  bash -c "$npm_bin install --legacy-peer-deps"
  if [ -d wiki ]; then rm -r wiki/*;
  else mkdir wiki; fi
  bash -c "$npm_bin run typedoc"
  if [ "$?" -ne "0" ]; then
    cat <<EOF
An error occurred while generating the documentation

EOF
    exit 1
  fi
fi

if test "x$flag_run_tests" = xyes; then

  cat <<EOF
Running tests...


EOF

if test "x$flag_use_docker" != xyes; then
  # Find npm and nodejs
  result=`npm -v > /dev/null 2>&1`
  if [ "$?" -ne "0" ]; then
    cat <<EOF
npm not found! Try installing it with \`sudo apt install npm\` Exitting...

EOF
    exit 1
  fi
  result=`node -v > /dev/null 2>&1`
  if [ "$?" -ne "0" ]; then
    cat <<EOF
node not found! Try installing it with \`sudo apt install nodejs\` Exitting...

EOF
    exit 1
  fi
fi
  cat <<EOF
  bash -c "$npm_bin install --legacy-peer-deps"
  bash -c "$npx_bin jest --runInBand"
EOF

  bash -c "$npm_bin install --legacy-peer-deps"

  bash -c "$npx_bin jest --runInBand"
  if [ "$?" -ne "0" ]; then
    cat <<EOF
An error occurred while running tests

EOF
    exit 1
  fi
fi

if test "x$flag_skip_building" = xyes; then
  cat << EOF
Skipping building due to the -s flag enabled
EOF
  exit 0;
fi

if test "x$flag_use_docker" != xyes; then
# Find TypeScript compiler
result=`tsc -v > /dev/null 2>&1`
if [ "$?" -ne "0" ]; then
  cat <<EOF
TypeScript compiler not found! Try installing it with \`sudo apt install node-typescript\` Exitting...

EOF
  exit 1
fi

cat <<EOF
Using $(whereis tsc)
EOF
fi

sources="src/*.ts"

if test "x$flag_with_examples" == xyes; then
  sources="$sources examples/**/*.ts examples/**/commands/*.ts examples/**/commands/**/*.ts"
fi

TSC_OPTIONS="$TSC_OPTIONS --outDir "$(pwd)/$OUTDIR" $sources"

  cat <<EOF
  bash -c "$npm_bin install --legacy-peer-deps"
EOF

bash -c "$npm_bin install --legacy-peer-deps"

cat <<EOF
$tsc_bin $TSC_OPTIONS
EOF
bash -c "$tsc_bin $TSC_OPTIONS"

if [ "$?" -ne "0" ]; then
  cat <<EOF
An error occured while compiling!

EOF
  exit 1
fi

  cat << EOF

Build process has succesfully ended. Search for the generated javascript files in the provided "$OUTDIR" directory.

EOF

exit 0
