/**
 * groups.test.ts - groups tests
 *
 * @since 0.0.1
 * @license GPLv3
 */

import { Command, CommandManager } from "../src/commands";
import { GroupManager } from "../src/groups";

describe("groups", () => {

  const command = new Command( "example", "", "Example command",
    [ { name: "arg1", type: "number" }, { name: "arg2", type: "number" } ], { type: "number" },
    ( arg1:number, arg2:number ):number => { return arg1 + arg2; }
  );

  const groupName = "My Group";
  const groupDescription = "My Group Description";
  const groupIndex = "mygroup";

  CommandManager.add( command );

  test("add group", () => {
    GroupManager.add( groupName, groupDescription, groupIndex );

    expect( GroupManager.has( groupIndex ) ).toBeTruthy();
  });

  test( "group integrity", () => {
    const group = GroupManager.get( groupIndex );

    expect( group ).not.toBeNull();

    expect( group.name ).toBe( groupName );
    expect( group.description ).toBe( groupDescription );
  });

  test("add command to group", () => {
    const group = GroupManager.get( groupIndex );
    expect( group.commands.length ).toBe( 0 );

    GroupManager.addCommand( groupIndex, command );

    expect( group.commands.length ).toBe( 1 );

    expect( group.commands[ 0 ].group() ).toBe( groupIndex );
  });

});