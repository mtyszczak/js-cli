/**
 * serialization.test.ts - serialization tests
 *
 * @since 0.0.1
 * @license GPLv3
 */

import { ArgumentReflector } from "../src/arguments";
import { registerPODs } from "../src/arguments_helper";

describe("POD arguments serialization", () => {

  registerPODs();

  test("bigint", () => {

    const bigIntString = "10123231739812132783129871832798132";
    const myBigInt = BigInt( bigIntString );

    const checker = ArgumentReflector.get<BigInt>( "bigint" );

    expect( checker.isValid( myBigInt ) ).toBeTruthy();
    expect( checker.toString( myBigInt ) ).toBe( bigIntString );
    expect( checker.fromString( bigIntString ) ).toBe( myBigInt );

  });

  test("boolean", () => {

    const boolTrue  = Boolean( true ),
          boolFalse = Boolean( false );

    const checker = ArgumentReflector.get<boolean>( "boolean" );

    expect( checker.isValid( boolTrue  ) ).toBeTruthy();
    expect( checker.isValid( boolFalse ) ).toBeTruthy();

    expect( checker.toString( boolTrue  ) ).toBe( "true"  );
    expect( checker.toString( boolFalse ) ).toBe( "false" );

    expect( checker.fromString( "true"  ) ).toBe( true  );
    expect( checker.fromString( "false" ) ).toBe( false );

  });

  test("number", () => {

    const myNumString = "12371283";
    const myNum = Number( myNumString );

    const checker = ArgumentReflector.get<number>( "number" );

    expect( checker.isValid( myNum ) ).toBeTruthy();
    expect( checker.toString( 12371283 ) ).toBe( myNumString );
    expect( checker.fromString( myNumString ) ).toBe( myNum );

  });

  test("string", () => {

    const myStr = "abcdefghijkl";

    const checker = ArgumentReflector.get<string>( "string" );

    expect( checker.isValid( myStr ) ).toBeTruthy();
    expect( checker.toString( myStr ) ).toBe( myStr );
    expect( checker.fromString( myStr ) ).toBe( myStr );

  });

  test("symbol", () => {

    const symbolStr = "abcdefghijkl";
    const symbol = Symbol( symbolStr );

    const checker = ArgumentReflector.get<Symbol>( "symbol" );

    expect( checker.isValid( symbol ) ).toBeTruthy();
    expect( checker.toString( checker.fromString( symbolStr ) ) ).toBe( symbolStr );

  });

  test("undefined", () => {

    const checker = ArgumentReflector.get<undefined>( "undefined" );

    expect( checker.isValid( undefined ) ).toBeFalsy();
    expect( checker.toString( undefined ) ).toBe( "undefined" );
    expect( checker.fromString( "undefined" ) ).toBe( undefined );

  });

  test("object", () => {

    const obj = {data:[{},{},{}]}
    const objStr = JSON.stringify(obj);

    const checker = ArgumentReflector.get<object>( "object" );

    expect( checker.isValid( null ) ).toBeFalsy();
    expect( checker.isValid( obj ) ).toBeTruthy();
    expect( checker.toString( obj ) ).toBe( objStr );
    expect( checker.fromString( objStr )[ "data" ].length ).toBe( obj.data.length );

  });

  test("function", () => {

    const func = () => {};

    const checker = ArgumentReflector.get<Function>( "function" );

    expect( checker.isValid( func ) ).toBeFalsy();
    expect( checker.toString( func ) ).toBe( "undefined" );
    expect( checker.fromString( "() => {}" ) ).toBe( undefined );

  });

  test("array", () => {

    const myArr = [{},{},{}];
    const myArrStr = JSON.stringify( myArr );

    const checker = ArgumentReflector.get<Array<any>>( "array" );

    expect( checker.isValid( myArr ) ).toBeTruthy();
    expect( checker.toString( myArr ) ).toBe( myArrStr );
    expect( checker.fromString( myArrStr ).length ).toBe( myArr.length );

  });

  test("void", () => {

    const checker = ArgumentReflector.get<void>( "void" );

    expect( checker.isValid( undefined ) ).toBeTruthy();
    expect( checker.toString( undefined ) ).toBe( "" );
    expect( checker.fromString( undefined ) ).toBe( undefined );

  });

  test("void", () => {

    const checker = ArgumentReflector.get<void>( "void" );

    expect( checker.isValid( undefined ) ).toBeTruthy();
    expect( checker.toString( undefined ) ).toBe( "" );
    expect( checker.fromString( undefined ) ).toBe( undefined );

  });

});