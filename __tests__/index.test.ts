/**
 * index.test.ts - specifies the order of tests
 *
 * @since 0.0.1
 * @license GPLv3
 */

require( "./serialization.test" );
require( "./commands.test" );
require( "./groups.test" );
