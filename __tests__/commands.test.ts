/**
 * commands.test.ts - commands tests
 *
 * @since 0.0.1
 * @license GPLv3
 */

import { ArgumentReflector } from "../src/arguments";
import { Command, CommandManager } from "../src/commands";

describe("commands", () => {

  const commandName = "example";
  const commandDescription = "Example command";
  const commandArgs = [ { name: "arg1", type: "number" }, { name: "arg2", type: "number" } ];
  const commandRet = { type: "number" };
  const commandInvoke = ( arg1:number, arg2:number ):number => { return arg1 + arg2; };

  test("register commands", () => {
    const command = new Command( commandName, "", commandDescription, commandArgs, commandRet, commandInvoke );

    CommandManager.add( command );

    expect( CommandManager.has( commandName ) ).toBeTruthy();
  });

  test( "command integrity", () => {
    const command = CommandManager.get( commandName );

    expect( command ).not.toBeNull();

    expect( command.argsList().length ).toBe( commandArgs.length );
    expect( command.returns().type ).toBe( commandRet.type );
    expect( command.description() ).toBe( commandDescription );
  });

  test( "command invoke", () => {
    const command = CommandManager.get( commandName );

    const checker = ArgumentReflector.get<number>("number");

    expect( CommandManager.invoke( commandName, "1", "2" ) ).toBe( "3" );
  });

});