/**
 * arguments_helper.ts - arguments types and interfaces helper
 *
 * @since 0.0.1
 * @license GPLv3
 */

import { ArgumentReflector } from "./arguments";

/**
 * Registers basic "POD"s for the user to let him use this basic param/return types
 * Includes: `bigint`, `boolean`, `number`, `string`, `symbol`, `undefined`, `object`, `function`, `Array`, `void`
 *
 * and some of their aliases: `Number`, `Function`, `Symbol`, `array`, `Object`
 */
export const registerPODs = () => {
  ArgumentReflector.set( "bigint",    { isValid( _data:bigint     ):boolean { return true;          }, toString(  data:bigint     ):string { return data.toString();             }, fromString(  data:string ):bigint     { return BigInt( data );            } } );
  ArgumentReflector.set( "boolean",   { isValid( _data:boolean    ):boolean { return true;          }, toString(  data:boolean    ):string { return data.toString();             }, fromString(  data:string ):boolean    { return data === "true";           } } );
  ArgumentReflector.set( "number",    { isValid( _data:number     ):boolean { return true;          }, toString(  data:number     ):string { return data.toString();             }, fromString(  data:string ):number     { return Number.parseFloat( data ); } } );
  ArgumentReflector.set( "string",    { isValid( _data:string     ):boolean { return true;          }, toString(  data:string     ):string { return data;                        }, fromString(  data:string ):string     { return data;                      } } );
  ArgumentReflector.set( "symbol",    { isValid( _data:symbol     ):boolean { return true;          }, toString(  data:symbol     ):string { return data.toString().slice(7,-1); }, fromString(  data:string ):symbol     { return Symbol.for( data );        } } );
  ArgumentReflector.set( "undefined", { isValid( _data:undefined  ):boolean { return false;         }, toString( _data:undefined  ):string { return "undefined";                 }, fromString( _data:string ):undefined  { return undefined                  } } );
  ArgumentReflector.set( "object",    { isValid(  data:object     ):boolean { return data !== null; }, toString(  data:object     ):string { return JSON.stringify( data );      }, fromString(  data:string ):object     { return JSON.parse(data);          } } );
   // You should not be able to pass any function to the program:
  ArgumentReflector.set( "function",  { isValid( _data:unknown    ):boolean { return false;         }, toString( _data:unknown    ):string { return "undefined";                 }, fromString( _data:string ):unknown    { return undefined;                 } } );
  ArgumentReflector.set( "array",     { isValid( _data:Array<any> ):boolean { return true;          }, toString(  data:Array<any> ):string { return JSON.stringify( data );      }, fromString(  data:string ):Array<any> { return JSON.parse(data);          } } );
  ArgumentReflector.set( "void",      { isValid( _data:void       ):boolean { return true;          }, toString( _data:void       ):string { return "";                          }, fromString( _data:string ):undefined  { return undefined;                 } } );
  // Aliases:
  ArgumentReflector.set( "Number",   ArgumentReflector.get("number")   );
  ArgumentReflector.set( "String",   ArgumentReflector.get("string")   );
  ArgumentReflector.set( "Symbol",   ArgumentReflector.get("symbol")   );
  ArgumentReflector.set( "Object",   ArgumentReflector.get("object")   );
  ArgumentReflector.set( "Array",    ArgumentReflector.get("array")    );
  ArgumentReflector.set( "Function", ArgumentReflector.get("function") );
};
