/**
 * groups.ts - command groups manager
 *
 * @since 0.0.1
 * @license GPLv3
 */

import { staticAssert } from "../libraries/js-helpers/src/assert";
import { Command, CommandManager } from "./commands";

/**
 * @interface
 */
export interface GroupManagerStorageIndexType {
  /**
   * Full name of the group
   * @type {string}
   */
  name: string;
  /**
   * Description of the group
   * @type {description}
   */
  description: string;
  /**
   * Array of commands that belong to this group
   * @type {Array< Command >}
   */
  commands: Array< Command >;
}

export type GroupModule = GroupManagerStorageIndexType;

/**
 * Basic Group Manager Storage type
 */
export type GroupManagerStorageType = {
  [ index:string ]: GroupManagerStorageIndexType
}

/**
 * Manages all of the command groups
 * @class
 */
export class GroupManager {
  /**
   * Command group manager storage
   * @private
   * @static
   */
  private static storage:GroupManagerStorageType = {} as GroupManagerStorageType;

  /**
   * @private
   * @constructor
   */
  private constructor() {}

  /**
   * Checks if given type is already registered in the CommandManager storage
   * @param {string} type type to be checked if exists
   * @returns {boolean} either true or false if type record exists
   * @public
   * @static
   */
  public static has( index:string ):boolean {
    return index in this.storage;
  }

  /**
   * Retrieves all of the storage keys (group names)
   * @returns {Array< string >} array of current storage keys
   * @public
   * @static
   */
  public static getStorageKeys():Array< string > {
    return Object.keys( this.storage );
  }

  /**
   * Adds group to the storage
   * @param {string} name group name
   * @param {string} description group description
   * @param {string} index index to be assigned for the group (defaults to the value of name)
   *
   * @throws {AssertionError} if storage already holds given index
   * @public
   * @static
   */
  public static add( name:string, description:string = "", index:string = name ) {
    staticAssert( !this.has( index ), "Storage already holds given index", { index } );
    this.storage[ index ] = { name, description, commands: [] } as GroupManagerStorageIndexType;
  }

  /**
   * Adds command to the gievn index
   *
   * Note: If command does not exist in CommandManager, this method will automatically register it
   * @param {string} index where should the command be added
   * @param {Command} command command to be added
   * @throws {AssertionError} if index does not exist on storage
   * @public
   * @static
   */
  public static addCommand( index:string, command:Command ) {
    staticAssert( this.has( index ), "Storage should hold given index in order to add a command", { index } );
    command.group( index );
    this.storage[ index ].commands.push( command );
    if( !CommandManager.has( command.name() ) )
      CommandManager.add( command );
  }

  /**
   * Retrieves storage elements by the given index
   * @param {string} index index identifying given storage element
   * @returns {GroupManagerStorageIndexType} storage element assigned to the given index
   * @throws {AssertionError} if not found
   * @public
   * @static
   */
  public static get( index:string ):GroupManagerStorageIndexType {
    staticAssert( this.has( index ), "Storage should hold given index in order to retrieve it", { index } );
    return this.storage[ index ];
  }

}
