/**
 * commands.ts - interactive program commands
 *
 * @since 0.0.1
 * @license GPLv3
 */

import { ArgumentReflector, ArgumentType } from "./arguments";
import { staticAssert } from "../libraries/js-helpers/src/assert";
import { GroupManager } from "./groups";

/**
 * Basic interface for registering new commands as modules in files
 * @interface
 */
export interface CommandModule {

  /**
   * command name
   * @type {string}
   */
   name:string;
   /**
    * command description
    * @type {string}
    */
   description:string;
   /**
    * command argument list
    * @type {Array<ArgumentType>}
    */
   argsList:Array< ArgumentType >;
   /**
    * command return data type
    * @type {ArgumentType}
    */
   returns: ArgumentType;
   /**
    * command invoke function
    * @type {Function}
    */
   invoke:Function;
};

/**
 * Basic command class
 * @class
 */
export class Command {
  /**
   * command name
   * @type {string}
   * @private
   */
  private name_:string;
  /**
   * command group
   * @type {string}
   * @private
   */
  private group_:string;
  /**
   * command description
   * @type {string}
   * @private
   */
  private description_:string;
  /**
   * command argument list
   * @type {Array<ArgumentType>}
   * @private
   */
  private argsList_:Array< ArgumentType >;
  /**
   * command return data type
   * @type {ArgumentType}
   * @private
   */
  private returns_: ArgumentType;
  /**
   * command invoke function
   * @type {Function}
   * @private
   */
  private invoke_: Function;

  /**
   * Checks integrity of the command
   * @throw {AssertionError} if any value is invalid
   */
  private checkValues() {
    staticAssert( this.name_.length > 0, "Command name must not be zero length" );
    staticAssert( ArgumentReflector.has( this.returns_.type ), "ArgumentReflector has to include the return type", { name: this.returns_.name, type: this.returns_.type } );
    for( const arg of this.argsList_ ) {
      staticAssert( "name" in arg, "Arguments should have names", { command_name: this.name_, args_list_type: arg.type } );
      staticAssert( ArgumentReflector.has( arg.type ), "ArgumentReflector has to include arg type", { command_name: this.name_, name: arg.name, type: arg.type } );
    }
  }

  /**
   * Retrieves the value
   * @returns {string} current value
   * @public
   */
  public name():string {
    return this.name_;
  }

  /**
   * Sets or retrieves the value
   *
   * Note: group is a group index not a group name!
   * @param {string|null} data data to be set or null to just retrieve the value (defaults to null)
   * @returns {string} current value
   * @public
   */
  public group( data:string|null = null ):string {
    return ( data !== null ? this.group_ = data : this.group_ );
  }

  /**
   * Retrieves the value
   * @returns {string} current value
   * @public
   */
  public description():string {
    return this.description_;
  }

  /**
   * Retrieves the value
   * @returns {Array<ArgumentType>} current value
   * @public
   */
  public argsList():Array<ArgumentType> {
    return this.argsList_;
  }

  /**
   * Retrieves the value
   * @returns {ArgumentType} current value
   * @public
   */
  public returns():ArgumentType {
    return this.returns_;
  }

  /**
   * Invokes this command function with given arguments and returns string representation of the return data
   * @param {any[]} args arguments to be passed to the function
   * @returns {string} string representation of the return data
   * @public
   */
  public invoke(...args:any[]):string {
    for( let i = 0; i < this.argsList_.length; ++i ) {
      if( args.length - 1 < i ) {
        staticAssert( "defaultValue" in this.argsList_[ i ], "No value specified for the \"${name}\" argument", { command_name: this.name_, i, args_length: args.length, args_list_length: this.argsList_.length, name: this.argsList_[ i ].name } );
        args.push( this.argsList_[ i ].defaultValue );
      }
      args[ i ] = ArgumentReflector.get( this.argsList_[ i ].type ).fromString( args[ i ] );
    }
    return ArgumentReflector.get( this.returns_.type ).toString( this.invoke_( ...args ) );
  }

  /**
   * Constructs new command with given values
   *
   * Adds this command to the specified group if group argument has anything inside
   * @param {string} name command name
   * @param {string} group group of the command
   * @param {string} description command description
   * @param {Array< ArgumentType >} argsList command arguments list
   * @param {ArgumentType} returns command return value
   * @param {Function} invoke command invoke function
   * @public
   */
  public constructor( name:string, group:string, description:string, argsList:Array< ArgumentType >, returns:ArgumentType, invoke:Function ) {
    this.name_ = name;
    this.group_ = group;
    this.description_ = description;
    this.argsList_ = argsList;
    this.returns_ = returns;
    this.invoke_ = invoke;

    this.checkValues();

    if( group.length )
      GroupManager.addCommand( group, this );
  }
}

/**
 * Basic Command Manager Storage type
 */
export type CommandManagerStorageType = {
  [ name:string ]: Command
}

/**
 * Parsed command type
 */
export type ParsedCommandType = {
  /**
   * Command name
   * @type {string}
   */
  name:string;
  /**
   * Command arguments
   * @type {Array<string>}
   */
  args:Array<string>;
}

/**
 * Command manager
 * @class
 */
export class CommandManager {
  /**
   * Command manager storage
   * @private
   * @static
   */
  private static storage:CommandManagerStorageType = {} as CommandManagerStorageType;

  /**
   * @private
   * @constructor
   */
  private constructor() {}

  /**
   * Checks if storage has command with specified name
   * @param {string} name name of the command to search
   * @returns {boolean} either true or false if storage has given command name
   * @public
   * @static
   */
  public static has( name:string ):boolean {
    return name in this.storage;
  }

  /**
   * Adds given command to the storage
   * @param {Command} command command to be added
   *
   * @public
   * @static
   */
  public static add( command:Command ) {
    this.storage[ command.name() ] = command;
  }

  /**
   * Retrieves all of the commands storage keys (command names)
   * @returns {Array< string >} array of current commands storage keys
   * @public
   * @static
   */
  public static getStorageKeys():Array< string > {
    return Object.keys( this.storage );
  }

  /**
   * Retrieves given Command from the storage
   * @param {string} name command name to retrieve
   * @returns {Command} command to be retrieved
   * @throws {AssertionError} if not found
   * @public
   * @static
   */
  public static get( name:string ):Command {
    staticAssert( this.has( name ), "Commands storage has to contain function with given name in order to retrieve it", {name} );
    return this.storage[ name ];
  }

  /**
   * Parsed given buf to the ParsedCommandType
   * @param {string} buf string to be parsed
   * @returns {ParsedCommandType} parsed data type
   * @see ParsedCommandType
   */
  public static parse( buf:string ):ParsedCommandType {
    let ret: ParsedCommandType = { name: "", args: [] } as ParsedCommandType;
    let bufArr:Array< string > = buf.trim().split( " " );
    let bufCopy:Array< string > = [];
    staticAssert( bufArr.length > 0, "User has to provide the command" );
    ret.name = bufArr.shift();
    let quoteIn:boolean = false;
    let tempStr:string = ``;
    for( const buf of bufArr ) {
      tempStr += buf; // Add no matter what a temp string
      if( !quoteIn && buf.charAt( 0 ) == '"' ) { // Start quote
        tempStr = tempStr.substr( 1 ); // Remove first quote mark
        quoteIn = true;
      } else if( quoteIn && buf.charAt( buf.length - 1 ) === '"' && buf.slice( -2 ) !== "\\\"" ) { // End a quote if last character is " and not \"
        tempStr = tempStr.slice( 0, -1 ); // Remove last quote mark
        quoteIn = false;
      }
      if( !quoteIn ) { // If is not in quote then push temp string into the arguments buffer copy
        bufCopy.push( tempStr );
        tempStr = ``; // Clear the string
      } else {
        tempStr += ' '; // If in quote than add space to the temp string due to the split before
      }
    }
    ret.args = bufCopy;
    staticAssert( !quoteIn, "Parse left in quote. Complete the quote in order to parse the command", { quoteIn, args: JSON.stringify( bufCopy ), buf } );
    return ret;
  }

  /**
   * Invokes given command with params
   * @param {string} name command name
   * @param {string[]} args additional arguments
   * @returns {string} string representation of the return data
   * @throws {AssertionError} if not found or on any error
   * @public
   * @static
   */
  public static invoke( name:string, ...args:string[] ):string {
    staticAssert( this.has( name ), "Commands storage has to contain function with given name in order to invoke it", {name,args} );
    return this.storage[ name ].invoke( ...args );
  }
}
