/**
 * commands_helper.ts - commands interface helper
 *
 * @since 0.0.1
 * @license GPLv3
 */

import fs = require("fs");
import path = require("path");
import chalk = require("chalk")
import { Command, CommandModule } from './commands';
import { GroupManager, GroupModule } from './groups';

/**
 * Retrieves commands from the group
 * @param {string} dirname__ absolute dirname of the caller
 * @param {string} filename__ relative dirname of the group folder of the caller
 * @returns {Array< Command >} parsed array of commands
 */
export const retrieveCommandsFromGroup = ( dirname__:string, filename__:string ):Array< Command > => {
  const fileName = path.parse( filename__ ).name;
  const dirents = fs.readdirSync( path.resolve( dirname__, fileName ), { withFileTypes: true } );
  const filesNames = dirents
      .filter(dirent => dirent.isFile())
      .map(dirent => dirent.name);
  let commands:Array< Command > = [];
  for( const file of filesNames ) {
    const cmod:CommandModule = require( path.resolve( dirname__, `${fileName}/${path.parse( file ).name}` ) ).default;
    commands.push( new Command( cmod.name, "", cmod.description, cmod.argsList, cmod.returns, cmod.invoke ) );
  }
  return commands;
};

/**
 * Registers all of the commands (as a standard layout -> root/groups/commands.ts) from the given dirname with relative filename
 * @param {string} dirname__ absolute dirname of the caller
 * @param {string} filename__ relative dirname of the root group folder of the caller
 */
export const registerAllCommands = ( dirname__:string, filename__:string ) => {
  const resolvePath = path.resolve(dirname__, filename__);
  const dirents = fs.readdirSync( resolvePath, { withFileTypes: true } );
  const filesNames = dirents
      .filter(dirent => dirent.isFile())
      .map(dirent => dirent.name);
  for( const file of filesNames ) {
    const groupIndex = path.parse( file ).name;
    const group:GroupModule = require( `${resolvePath}/${groupIndex}` ).default;
    GroupManager.add( group.name, group.description, groupIndex );
    for( const command of group.commands ) {
      command.group( groupIndex );
      GroupManager.addCommand( groupIndex, command );
    }
  }
};

/**
 * Generates help for given message
 * @param {Command} command command from which the help will be generated
 * @returns {string} generated help
 */
export const deserializeCommandUsage = (command:Command):string => {
  let ret = chalk.bold( command.name() );
  ret += '(';
  let argCounter = 0;
  for( const arg of command.argsList() ) {
    ret += ` ${arg.name}: ${arg.type}`;
    if( "defaultValue" in arg )
      ret += chalk.italic( ` = ${arg.defaultValue}` );
    ret += ',';
    ++argCounter;
  }
  if( argCounter > 0 ) {
    ret = ret.slice( 0, -1 );
    ret += ' ';
  }
  ret += `): ${command.returns().type}`;
  return ret;
};
