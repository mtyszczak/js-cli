/**
 * arguments.ts - arguments types and interfaces
 *
 * @since 0.0.1
 * @license GPLv3
 */

import { staticAssert } from "../libraries/js-helpers/src/assert";
import { registerPODs } from "./arguments_helper";

/**
 * Basic argument type
 * @interface
 */
export interface ArgumentType {
  /**
   * name of the argument
   * optional only for return arguments
   * @type {string}
   */
  name?: string;
  /**
   * typeof argument. Can be custom user defined type
   * @type {string}
   */
  type: string;
  /**
   * if specified than argument is marked as optional
   * @type {any}
   */
  defaultValue?: any;
}

/**
 * Declares how to check integrity of the reflected data types
 * @interface
 */
export interface ArgumentReflectorChecker<T> {
  /**
   * Checks integrity of the given data
   * @param {T} data data to be checked
   * @returns {boolean} either true or false if given type is valid
   */
  isValid( data:T ):boolean;

  /**
   * Converts given data to the string representation
   * @param {T} data data to be converted
   * @returns {string} converted string
   */
  toString( data:T ):string;

  /**
   * Constructs the type from string
   * @param {string} data data to be converted
   * @returns {T} serialized data
   */
  fromString( data:string ):T;
}

/**
 * Basic Argument Reflector Storage type
 */
export type ArgumentReflectorStorageType = {
  [ type:string ]: ArgumentReflectorChecker<unknown>
}

/**
 * Reflects all of the arguments in order to later check them on runtime
 * @class
 */
export class ArgumentReflector {
  /**
   * Argument reflector storage
   * @private
   * @static
   */
  private static storage:ArgumentReflectorStorageType = {} as ArgumentReflectorStorageType;

  /**
   * @private
   * @constructor
   */
  private constructor() {}

  /**
   * Checks if given type is already registered in the ArgumentReflector storage
   * @param {string} type type to be checked if exists
   * @returns {boolean} either true or false if type record exists
   * @public
   * @static
   */
  public static has( type:string ):boolean {
    return type in this.storage;
  }

  /**
   * Retrieves all of the storage keys (argument types reflected)
   * @returns {Array< string >} array of current storage keys
   * @public
   * @static
   */
  public static getStorageKeys():Array< string > {
    return Object.keys( this.storage );
  }

  /**
   * Sets or adds given type with a checker to the storage
   * @param {string} type type name to be assigned
   * @param {ArgumentReflectorChecker< T >} checker type checker and converter to be used
   * @public
   * @static
   */
  public static set<T>( type:string, checker:ArgumentReflectorChecker< T > ) {
    this.storage[ type ] = checker;
  }

  /**
   * Returns argument reflector checker assigned to the given type name
   * @param {string} type type name from the storage
   * @returns {ArgumentReflectorChecker< T >} corresponding argument reflector checker
   *
   * @throws {AssertionError} if not found
   * @public
   * @static
   */
  public static get<T>( type:string ):ArgumentReflectorChecker< T > {
    staticAssert( this.has( type ), "Storage should hold given type in order to retrieve it", { type } );
    return this.storage[ type ] as unknown as ArgumentReflectorChecker< T >;
  }
}

// Needed for basic ArgumentRefletorChecker usage
registerPODs();
